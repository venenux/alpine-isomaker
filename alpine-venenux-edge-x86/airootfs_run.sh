#!/usr/bin/env bash

# ====================================
#defaults base
apk add attr attr-doc procps procps-doc binutils findutils readline 
echo -e "root\nroot\n" | passwd root
# =====================================
# console base
apk add man-pages man-db doas
apk add wget wget-doc bash bash-doc less groff bzip2 lsof nano coreutils coreutils-doc sed attr procps dialog binutils findutils readline curl curl-doc perl perl-utils perl-doc
# console admin tools
apk add pciutils pciutils-doc util-linux util-linux-doc coreutils coreutils-doc bash bash-doc less less-doc groff groff-doc lsof lsof-doc sed sed-doc
apk add aria2 aria2-doc curl curl-doc wget wget-doc
apk add htop htop-doc mc mc-doc rsync rsync-doc git git-cvs git-svn git-doc subversion subversion-doc nano perl perl-utils perl-doc
apk add unzip bzip2 p7zip xz libarchive-tools libarchive-doc
apk add fortune
# ====================================
# device management, video and networking base
apk add dbus dbus-openrc dbus-doc eudev eudev-openrc eudev-doc udevil udevil-doc acpi acpi-doc eudev-rule-generator
rc-update add udev
rc-update add dbus
# networking need in teaiso a network manager. currently only networkmanager works well, it depends on dbus
apk add networkmanager networkmanager-openrc wpa_supplicant wpa_supplicant-doc
rc-update add networkmanager
rc-update add wpa_supplicant
# hardware network management
apk add iw nmap nmap-scripts nmap-ncat nmap-doc nmap-nping fping fping-doc wpa_supplicant wpa_supplicant-doc snmptt net-snmp net-snmp-tools net-snmp-doc
# hardware sound management
apk add alsa-utils alsa-utils-doc alsa-lib alsaconf
rc-update add alsa
# hardware disk management
apk add partimage partimage-doc ntfs-3g ntfs-3g-doc ntfs-3g-progs sfdisk cfdisk gptfdisk gptfdisk-doc sgdisk
